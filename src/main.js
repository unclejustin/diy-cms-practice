import Vue from 'vue'
import VueMeta from 'vue-meta'
import App from './App.vue'
import { createRouter } from './router'
import { createStore } from './store'
import '@/assets/css/tailwind.css'

Vue.config.productionTip = false
Vue.use(VueMeta)

export async function createApp({
  beforeApp = () => {},
  afterApp = () => {},
} = {}) {
  const router = createRouter()
  const store = createStore()

  await beforeApp({
    router,
    store,
  })

  const app = new Vue({
    router,
    store,
    render: h => h(App),
  })

  const result = {
    app,
    router,
    store,
  }

  await afterApp(result)

  return result
}
