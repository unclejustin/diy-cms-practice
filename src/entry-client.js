import { loadAsyncComponents } from '@akryum/vue-cli-plugin-ssr/client'
import routes from 'vue-auto-routing'
import { createRouterLayout } from 'vue-router-layout'

import { createApp } from './main'

createApp({
  async beforeApp({ router }) {
    await loadAsyncComponents({ router })
  },

  afterApp({ app, router, store }) {
    store.replaceState(window.__INITIAL_STATE__)
    router.onReady(() => {
      const RouterLayout = createRouterLayout(layout => {
        return import('@/layouts/' + layout + '.vue')
      })
      router.addRoutes([
        {
          path: '/',
          component: RouterLayout,
          children: routes,
        },
      ])
      app.$mount('#app')
    })
  },
})
