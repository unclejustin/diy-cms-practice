const fetch = require('node-fetch')
const repoId = '19634647'
const path = 'src/db/'.replace(/\//g, '%2F')

exports.handler = async function({ body }) {
  const { content, type } = JSON.parse(body)
  try {
    const url = `https://gitlab.com/api/v4/projects/${repoId}/repository/files/${path +
      type}.json`
    console.log('url', url)
    const response = await fetch(url, {
      method: 'PUT',
      body: JSON.stringify({
        commit_message: 'Update posts',
        branch: 'master',
        author_name: 'CMS',
        content,
      }),
      headers: {
        'PRIVATE-TOKEN': process.env.GL_PRIVATE_TOKEN,
        'Content-Type': 'application/json',
      },
    })
    if (!response.ok) {
      // NOT res.status >= 200 && res.status < 300
      return { statusCode: response.status, body: response.statusText }
    }
    const msg = await response.json()

    return {
      statusCode: 200,
      body: JSON.stringify({ msg }),
    }
  } catch (err) {
    console.log(err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: err.message }),
    }
  }
}
